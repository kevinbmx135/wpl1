﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfOpdrachtGit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

       

        private void Img01_MouseEnter(object sender, MouseEventArgs e)
        {
            WPF.Background = Brushes.Orange; 
        }

        private void Img02_MouseEnter(object sender, MouseEventArgs e)
        {
            WPF.Background = Brushes.Purple;
        }

        private void Img03_MouseEnter(object sender, MouseEventArgs e)
        {
            WPF.Background = Brushes.Yellow;
        }

        private void Img04_MouseEnter(object sender, MouseEventArgs e)
        {
            WPF.Background = Brushes.Red;
        }
        private void BcakgroundNormaal()
        {
            WPF.Background = Brushes.LightGray;
        }

        private void Img04_MouseLeave(object sender, MouseEventArgs e)
        {
            BcakgroundNormaal();
        }
        
        private void Img03_MouseLeave(object sender, MouseEventArgs e)
        {
            BcakgroundNormaal();
        }

        private void Img02_MouseLeave(object sender, MouseEventArgs e)
        {
            BcakgroundNormaal();
        }

        private void Img01_MouseLeave(object sender, MouseEventArgs e)
        {
            BcakgroundNormaal();
        }
    }
}
